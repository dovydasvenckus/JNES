package com.dovydasvenckus.jnes.math

import com.dovydasvenckus.jnes.type.UnsignedByte
import spock.lang.Specification

class UnsignedMathSpec extends Specification{


    def 'not operation should work'(byte byteValue, int expectedInt) {
        expect:
        UnsignedMath.not(byteValue) == expectedInt

        where:
        byteValue | expectedInt
        0x00  | 0x000000ff
        0x01  | 0x000000fe
        0x0f  | 0x000000f0
        0xf0  | 0x0000000f
        0xff  | 0x00000000
    }

    def 'or should work as expected'(byte byteValue, byte orValue, int expectedInt) {
        given:
        UnsignedByte unsignedByte = new UnsignedByte(byteValue)

        when:
        unsignedByte.or(new UnsignedByte(orValue))

        then:
        unsignedByte.toInt() == expectedInt

        where:
        byteValue       | orValue       | expectedInt
        0b00000000  | 0b00000000    | 0
        0b00000000  | 0b11111111    | 0x000000ff
        0b00001111  | 0b11110000    | 0x000000ff
        0b00000001  | 0b00000010    | 0x00000003
        0b01010101  | 0b10101010    | 0x000000ff
    }

    def 'and should work as expected'(byte byteValue, byte andValue, int expectedInt){
        given:
        UnsignedByte unsignedByte = new UnsignedByte(byteValue)

        when:
        unsignedByte.and(new UnsignedByte(andValue))

        then:
        unsignedByte.toInt() == expectedInt

        where:
        byteValue       | andValue      | expectedInt
        0b00000000      | 0b11111111    | 0
        0b00000001      | 0b00000001    | 1
        0b11111111      | 0b11111111    | 255
        0b00001111      | 0b00000111    | 7
    }

    def 'xor should work as expected'(byte byteValue, byte xorValue, int expectedInt){
        given:
        UnsignedByte unsignedByte = new UnsignedByte(byteValue)

        when:
        unsignedByte.xor(new UnsignedByte(xorValue))

        then:
        unsignedByte.toInt() == expectedInt

        where:
        byteValue       | xorValue      | expectedInt
        0b00000000      | 0b11111111    | 0x000000ff
        0b00000000      | 0b00000000    | 0x00000000
        0b11110000      | 0b00000000    | 0x000000f0
        0b11110000      | 0b00001111    | 0x000000ff
        0b11111111      | 0b11111111    | 0x00000000
        0b10101010      | 0b10101010    | 0x00000000
        0b10101010      | 0b01010101    | 0x000000ff
    }
}
