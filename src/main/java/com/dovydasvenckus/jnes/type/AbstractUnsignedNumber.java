package com.dovydasvenckus.jnes.type;

public abstract class AbstractUnsignedNumber implements UnsignedNumber{
    protected int number;

    @Override
    public int toInt() {
        maskUnusedBits();
        return number;
    }

    @Override
    public String toString() {
        maskUnusedBits();
        return Integer.toBinaryString(number);
    }

    public abstract void maskUnusedBits();

}
