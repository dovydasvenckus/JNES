package com.dovydasvenckus.jnes.type;

public interface UnsignedNumber {
    void not();

    void or(AbstractUnsignedNumber abstractUnsignedNumber);

    void xor(AbstractUnsignedNumber abstractUnsignedNumber);

    void and(AbstractUnsignedNumber abstractUnsignedNumber);

    int toInt();
}
