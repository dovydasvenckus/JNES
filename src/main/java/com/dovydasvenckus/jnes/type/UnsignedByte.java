package com.dovydasvenckus.jnes.type;

public class UnsignedByte extends AbstractUnsignedNumber{

    public UnsignedByte(byte number) {
        this.number = number;
    }

    @Override
    public void maskUnusedBits() {
        number &= 0x000000ff;
    }

    @Override
    public void not() {
        number = ~number;
        maskUnusedBits();
    }

    @Override
    public void or(AbstractUnsignedNumber abstractUnsignedNumber) {
        number |= abstractUnsignedNumber.toInt();
    }

    @Override
    public void xor(AbstractUnsignedNumber abstractUnsignedNumber) {
        number ^= abstractUnsignedNumber.toInt();
    }

    @Override
    public void and(AbstractUnsignedNumber abstractUnsignedNumber) {
        number &= abstractUnsignedNumber.toInt();
    }
}
