package com.dovydasvenckus.jnes.math;

public class UnsignedMath {

    private static int maskUnusedBytes(int number) {
        return number &= 0x000000ff;
    }

    public static byte not(byte number) {
        int intValue = maskUnusedBytes(number);
        intValue =~ intValue;
        return (byte) maskUnusedBytes(intValue);
    }

    public static byte or(byte first, byte second) {
        return first |= second;
    }

    public static byte xor(byte first, byte second) {
        return first ^= second;
    }

    public static byte and(byte first, byte second) {
        return first &= second;
    }

    public static int toInt(byte number) {
        return number;
    }

    public static String toString(byte number) {
        maskUnusedBytes((int) number);
        return Integer.toBinaryString(number);
    }
}
